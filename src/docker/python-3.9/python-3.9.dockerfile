ARG BASE_IMAGE='python'
ARG BASE_IMAGE_TAG='3.9-slim'
FROM $BASE_IMAGE:$BASE_IMAGE_TAG

LABEL maintainer="Luca Molari <molari.luca@gmail.com>"

ARG CONTAINER_USER_UID=1000
ARG CONTAINER_USER_NAME='container_user'
ARG CONTAINER_USER_GID=1000
ARG CONTAINER_USER_GROUP='container_group'
ARG PIP_TIMEOUT=1000

ENV CONTAINER_USER_UID="${CONTAINER_USER_UID}"
ENV CONTAINER_USER_NAME="${CONTAINER_USER_NAME}"
ENV CONTAINER_USER_GID="${CONTAINER_USER_GID}"
ENV CONTAINER_USER_GROUP="${CONTAINER_USER_GROUP}"
ENV PIP_TIMEOUT="${PIP_TIMEOUT}"
ENV PYTHONPATH="/srv/lib/:${PYTHONPATH}"

SHELL ["/bin/bash", "-euo", "pipefail", "-c"]

USER 0
RUN set -x; \
  id

RUN set -x; \
  groupadd "${CONTAINER_USER_GROUP}" -g "${CONTAINER_USER_GID}" ; \
  useradd "${CONTAINER_USER_NAME}" -u "${CONTAINER_USER_UID}" -g "${CONTAINER_USER_GID}" -m -s '/bin/bash'

RUN set -x; \
  mkdir -p '/srv/assets/' ; \
  mkdir -p '/srv/bin/' ; \
  mkdir -p '/srv/cfg/' ; \
  mkdir -p '/srv/lib/' ; \
  mkdir -p '/srv/run/' ; \
  mkdir -p '/srv/run/log/' ; \
  mkdir -p '/srv/run/shared/' ; \
  mkdir -p '/srv/secrets/' ; \
  chown -R "${CONTAINER_USER_NAME}:${CONTAINER_USER_GROUP}" '/srv'

RUN set -x; \
  pip install --no-cache-dir \
    --default-timeout="${PIP_TIMEOUT}" \
    --upgrade pip

RUN set -x; \
  pip freeze --all; \
  pip check

USER "${CONTAINER_USER_NAME}"
RUN set -x; \
  id

COPY './docker/python-3.9/python-3.9.requirements.txt' '/srv/lib/'

RUN set -x; \
  pip install --no-cache-dir \
    --default-timeout="${PIP_TIMEOUT}" \
    -r '/srv/lib/python-3.9.requirements.txt'

RUN set -x; \
  mkdir -p "/home/${CONTAINER_USER_NAME}/.local/bin/"

ENV PATH="/home/${CONTAINER_USER_NAME}/.local/bin:/srv/bin:${PATH}"

WORKDIR '/srv/'
