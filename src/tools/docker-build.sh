#!/usr/bin/env bash

# Build a Docker Image.

set -euo pipefail;

# Constants.

IMAGE_NAME="$1"
FULL_IMAGE="$2"
IMAGE_TAG="$3"

# Checks.

stat 'project-root.dir' > /dev/null

# Main.

set -x

exec docker build \
  --pull \
  './src/' \
  -f "./src/docker/${IMAGE_NAME}/${IMAGE_NAME}.dockerfile" \
  -t "${FULL_IMAGE}:${IMAGE_TAG}"
